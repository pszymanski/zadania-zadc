# -*- coding: utf-8 -*-

import json
from time import sleep
from pprint import pprint
from MapReduce import MapReduce


def map(item):
    key=int(item[1])
    del item[1]
    yield key, item


def reduce(key, values):
    out=[]
    for line in values[1:]:
        out.append([str(key)]+values[0]+line)
    return out


if __name__ == '__main__':
    data = []
    with open('records.json') as f:
        for line in f:
            data.append(json.loads(line))
    input_data = data
    mapper = MapReduce(map, reduce)
    results = mapper(input_data)
    for row in results:
        for line in row:
            print line
    with open('out.json','w+') as f:
        for row in results:
            for line in row:
                f.write(str(line)+"\n")
